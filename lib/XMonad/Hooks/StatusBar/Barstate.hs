module XMonad.Hooks.StatusBar.Barstate where

import Data.Barstate (FullStatus(..), ScreenStatus(..), WorkspaceStatus(..))
import Data.List (find, sortBy)
import Data.Maybe (isJust, fromMaybe)

import XMonad (X, Window, WindowSet, WindowSpace, LayoutClass, description, windowset, gets)
import qualified XMonad.StackSet as S

import XMonad.Hooks.UrgencyHook (readUrgents)

import XMonad.Util.NamedWindows (getName)
import XMonad.Util.WorkspaceCompare (WorkspaceSort, getSortByIndex)


-- * `FullStatus`
-- | Build the full status from the current state of XMonad.
getFullStatus :: X FullStatus
getFullStatus = do
  winset  <- gets windowset
  urgents <- readUrgents
  -- use the default sorter that sorts in the order of 'workspaces'.
  sorter <- getSortByIndex

  screenStatus <- toScreenStatusList winset

  pure $ FullStatus { fs_workspaces = toWorkspaceStatusList sorter urgents winset
                    , fs_screens = screenStatus
                    }

-- * `ScreenStatus`
-- | Build a `ScreenStatus` for a single screen.
toScreenStatus :: (LayoutClass l a, Integral sid) => S.Screen String (l a) a sid sd -> String -> ScreenStatus
toScreenStatus s wt = ScreenStatus { sc_sid = fromIntegral $ S.screen s
                                   , sc_tag = S.tag ws
                                   , sc_layout = description $ S.layout ws
                                   , sc_windowTitle = wt
                                   }
                                where
                                  ws = S.workspace s

-- | Build a complete list of all screens and the associated information.
-- The list is sorted by the screen Xinerama ID, in ascending order.
-- The window titles are looked up using access to the X monad.
toScreenStatusList :: WindowSet -> X [ScreenStatus]
toScreenStatusList ws =
  mapM (\screen -> do
           winTitle <- maybe (pure "") (fmap show . getName . S.focus) . S.stack . S.workspace $ screen
           pure $ toScreenStatus screen winTitle) sortedScreens
  where
    sortedScreens = sortBy (\s1 s2 -> compare (S.screen s1) (S.screen s2)) $ S.screens ws

-- * `WorkspaceStatus`
-- | Get list of workspaces from the WindowSet/StackSet.
toWorkspaceStatus :: [Window] -> WindowSet -> WindowSpace -> WorkspaceStatus
toWorkspaceStatus urgents winset (S.Workspace tag layout' winstack) = WorkspaceStatus
  { ws_name = tag
  , ws_layout = description layout'
  , ws_isCurrent = tag == S.currentTag winset
  , ws_visibleOn = visibleOnSid
  , ws_hasWindows = isJust winstack
  , ws_isUrgent = any (\x -> Just tag == (S.findTag x winset)) urgents
  } where
    visibleOnSid = fromMaybe (-1) $ (fromIntegral . S.screen)
                <$> find (\screen -> tag == (S.tag . S.workspace) screen) screens

    screens = (S.current winset : S.visible winset)

-- | Build a complete list of all workspaces and their statuses, sorting them according to a `WorkspaceSort`er.
toWorkspaceStatusList :: WorkspaceSort -> [Window] -> WindowSet -> [WorkspaceStatus]
toWorkspaceStatusList sorter urgents ws = map (toWorkspaceStatus urgents ws) . sorter $ S.workspaces ws

