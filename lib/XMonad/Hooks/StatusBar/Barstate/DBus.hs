module XMonad.Hooks.StatusBar.Barstate.DBus where

import Control.Monad (when)
import Data.Maybe (isNothing, fromJust)

import qualified DBus.Client as DC
import qualified DBus.Internal.Types as DT
import qualified DBus.Internal.Message as DM
import qualified DBus.Introspection.Types as DIT

import XMonad (X, ScreenId(..), ExtensionClass(..), io)
import XMonad.Hooks.StatusBar (StatusBarConfig(..), spawnStatusBar, killStatusBar)
import XMonad.Hooks.StatusBar.Barstate (getFullStatus)
import qualified XMonad.Util.ExtensibleState as XS

data Barstate = Barstate { bs_dbusClient :: Maybe DC.Client
                         }

instance ExtensionClass Barstate where
  initialValue = Barstate { bs_dbusClient = Nothing
                          }

-- * Config
-- | Helper for use with `XMonad.Hooks.StatusBar.dynamicSBs`.
--
-- Example:
-- > main = xmonad $ dynamicSBs (statusBarDBus (\sid -> "xmonad -x" ++ show sid)) $ def { ... }
statusBarDBus :: (Int -> String)        -- ^ Function that takes an unwrapped `ScreenId` and returns a command to start an instance of the bar.
              -> ScreenId               -- ^ `ScreenId` to launch the bar on.
              -> X StatusBarConfig
statusBarDBus barCmdFunc (S sid) =
  pure $ StatusBarConfig { sbLogHook =
                             -- Add only one log hook to avoid multiple updates.
                             -- TODO/HACK: As far as I know, there is no guarantee that Screen 0 will always be present.
                             -- This would mean that if someone disconnects their primary display,
                             -- logging will seize as the corresponding bar is removed.
                             if sid > 0 then mempty else barstateLogHookDBus
                         , sbStartupHook = do
                             barstate <- XS.get
                             when (isNothing $ bs_dbusClient barstate)
                               (do client <- io initDBusClient
                                   XS.put $ barstate { bs_dbusClient = Just client })
                             spawnStatusBar (barCmdFunc sid)
                         , sbCleanupHook = killStatusBar (barCmdFunc sid)
                         }

-- * Hooks
barstateLogHookDBus :: X ()
barstateLogHookDBus = do
  barstate <- XS.get
  status <- getFullStatus
  io $ DC.emit (fromJust $ bs_dbusClient barstate) DM.Signal { DM.signalPath = dbusClientPath
                                                             , DM.signalInterface = dbusInterfaceName
                                                             , DM.signalMember = dbusSignalName
                                                             , DM.signalSender = Nothing
                                                             , DM.signalDestination = Nothing
                                                             , DM.signalBody = [ DT.toVariant status ]
                                                             }

-- * Helpers
initDBusClient :: IO DC.Client
initDBusClient = do
  c <- DC.connectSession
  -- Allow replacement, since the last owner will not be removed on a restart of XMonad.
  -- This is because XMonad replaces itself on a restart, leaving the pid the same, but deleting the old client.
  requestResult <- DC.requestName c dbusClientName [DC.nameReplaceExisting, DC.nameAllowReplacement]
  when (requestResult /= DC.NamePrimaryOwner)
    $ error ("Service already in use: " ++ show requestResult)
  DC.export c dbusClientPath interface
  pure c
  where
    interface = DC.defaultInterface { DC.interfaceName = dbusInterfaceName
                                    , DC.interfaceSignals = [ signal ]
                                    }
    signal = DIT.Signal { DIT.signalName = dbusSignalName
                        , DIT.signalArgs = [ signalArg ]
                        }
    signalArg = DIT.SignalArg { DIT.signalArgName = "data"
                              , DIT.signalArgType = signalArgType
                              }
    -- TODO: The typing using Variants is currently a bit broken, as is basically performs type erasure.
    -- Here, we assemble the (proper) type by hand, until I have time to do a cleaner implementation.
    signalArgType =
      -- FullStatus
      DT.TypeStructure
        -- [WorkspaceStatus]
        [ DT.TypeArray $ DT.TypeStructure
           -- ws_name
          [ DT.TypeString
          -- ws_layout
          , DT.TypeString
          -- ws_isCurrent
          , DT.TypeBoolean
          -- ws_visibleOn
          , DT.TypeInt16
          -- ws_hasWindows
          , DT.TypeBoolean
          -- ws_isUrgent
          , DT.TypeBoolean
          ]
        -- [ScreenStatus]
        , DT.TypeArray $ DT.TypeStructure
          -- sc_sid
          [ DT.TypeInt16
          -- sc_tag
          , DT.TypeString
          -- sc_layout
          , DT.TypeString
          -- sc_windowTitle
          , DT.TypeString
          ]
        ]

-- * Constants
-- | DBus client name, defaults to @org.finn.barstate@.
dbusClientName :: DT.BusName
dbusClientName = DT.BusName "org.finn.barstate"

-- | DBus client path, defaults to @\/org\/finn\/barstate@.
dbusClientPath :: DT.ObjectPath
dbusClientPath = DT.ObjectPath "/org/finn/barstate"

-- | DBus name of the primary interface, defaults to @org.finn.barstate@.
dbusInterfaceName :: DT.InterfaceName
dbusInterfaceName = DT.InterfaceName "org.finn.barstate"

-- | DBus name of the update signal in the primary interface, defaults to @update@.
dbusSignalName :: DT.MemberName
dbusSignalName = DT.MemberName "update"
