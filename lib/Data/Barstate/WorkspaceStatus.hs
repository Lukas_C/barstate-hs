#if json
{-# LANGUAGE TemplateHaskell #-}
#endif
{-# LANGUAGE TypeApplications #-}

module Data.Barstate.WorkspaceStatus where

#if dbus
import Data.Int (Int16)
import DBus.Internal.Types
#endif

#if json
import Data.Aeson
import Data.Aeson.TH
#endif

import GHC.Generics


-- | Specifies the status of a single workspace.
data WorkspaceStatus = WorkspaceStatus { ws_name :: String     -- ^ Name of the workspace
                                       , ws_layout :: String   -- ^ Currently used layout
                                       , ws_isCurrent :: Bool  -- ^ The workspace is currently focused
                                       , ws_visibleOn :: Int   -- ^ The workspace is visible on a screen, smaller than zero means not visible.
                                                               -- May be truncated to an `Data.Int.Int16` by the backing implementation.
                                       , ws_hasWindows :: Bool -- ^ The workspace contains one or more windows
                                       , ws_isUrgent :: Bool   -- ^ The workspace contains an urgent window
                                       } deriving (Generic, Read, Show, Eq)

#if dbus
instance IsVariant WorkspaceStatus where
  toVariant v = Variant $ ValueStructure [ toValue $ ws_name v
                                         , toValue $ ws_layout v
                                         , toValue $ ws_isCurrent v
                                         , toValue $ fromIntegral @Int @Int16 $ ws_visibleOn v
                                         , toValue $ ws_hasWindows v
                                         , toValue $ ws_isUrgent v
                                         ]

  fromVariant (Variant (ValueStructure [name, layout, isCurrent, visibleOn, hasWindows, isUrgent])) = do
    ws_name <- (fromVariant . Variant) name
    ws_layout <- (fromVariant . Variant) layout
    ws_isCurrent <- (fromVariant . Variant) isCurrent
    ws_visibleOn <- fromIntegral @Int16 @Int <$> (fromVariant . Variant) visibleOn
    ws_hasWindows <- (fromVariant . Variant) hasWindows
    ws_isUrgent <- (fromVariant . Variant) isUrgent
    pure $ WorkspaceStatus { ws_name
                           , ws_layout
                           , ws_isCurrent
                           , ws_visibleOn
                           , ws_hasWindows
                           , ws_isUrgent
                           }
  fromVariant _ = Nothing
#endif

#if json
$(deriveJSON defaultOptions{fieldLabelModifier = drop 3} ''WorkspaceStatus)
#endif
