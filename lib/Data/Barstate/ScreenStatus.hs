#if json
{-# LANGUAGE TemplateHaskell #-}
#endif
{-# LANGUAGE TypeApplications #-}

module Data.Barstate.ScreenStatus where

#if dbus
import Data.Int (Int16)
import DBus.Internal.Types
#endif

#if json
import Data.Aeson
import Data.Aeson.TH
#endif

import GHC.Generics


-- | Specifies the status of a single screen.
data ScreenStatus = ScreenStatus { sc_sid :: Int            -- ^ Xinerama ID of the screen.
                                                            -- May be truncated to an `Data.Int.Int16` by the backing implementation.
                                 , sc_tag :: String         -- ^ Name of the currently visible workspace
                                 , sc_layout :: String      -- ^ Name of the currently used layout
                                 , sc_windowTitle :: String -- ^ Title of the currently focused window
                                 } deriving (Generic, Read, Show, Eq)

#if dbus
instance IsVariant ScreenStatus where
  toVariant v = Variant $ ValueStructure [ toValue $ fromIntegral @Int @Int16 $ sc_sid v
                                         , toValue $ sc_tag v
                                         , toValue $ sc_layout v
                                         , toValue $ sc_windowTitle v
                                         ]
  fromVariant (Variant (ValueStructure [sid, tag, layout, windowTitle])) = do
    sc_sid <- fromIntegral @Int16 @Int <$> (fromVariant . Variant) sid
    sc_tag <- (fromVariant . Variant) tag
    sc_layout <- (fromVariant . Variant) layout
    sc_windowTitle <- (fromVariant . Variant) windowTitle
    pure $ ScreenStatus { sc_sid
                        , sc_tag
                        , sc_layout
                        , sc_windowTitle
                        }
  fromVariant _ = Nothing
#endif

#if json
$(deriveJSON defaultOptions{fieldLabelModifier = drop 3} ''ScreenStatus)
#endif
