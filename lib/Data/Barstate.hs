#if json
{-# LANGUAGE TemplateHaskell #-}
#endif

module Data.Barstate
  ( FullStatus(..)
  , ScreenStatus(..)
  , WorkspaceStatus(..)
#if json
  , toJsonString
  , fromJsonString
#endif
  ) where

#if dbus
import Control.Monad ((<=<))
import DBus.Internal.Types
#endif

#if json
import Data.Aeson
import Data.Aeson.TH
import qualified Data.ByteString.Lazy.UTF8 as B
#endif

import Data.Barstate.ScreenStatus
import Data.Barstate.WorkspaceStatus

import GHC.Generics


-- | Combined `[WorkspaceStatus]` and `[ScreenStatus]`.
data FullStatus = FullStatus { fs_workspaces :: [WorkspaceStatus]
                             , fs_screens :: [ScreenStatus]
                             } deriving (Generic, Read, Show, Eq)

#if dbus
instance IsVariant FullStatus where
  toVariant v = Variant $ ValueStructure [ toValue $ map toVariant $ fs_workspaces v
                                         , toValue $ map toVariant $ fs_screens v
                                         ]

  fromVariant (Variant (ValueStructure [workspaces, screens])) = do
    -- [WorkspaceStatus] <- [Variant WorkspaceStatus] <- Variant [Variant WorkspaceStatus]<- Value
    fs_workspaces <- mapM fromVariant <=< fromVariant . Variant $ workspaces
    fs_screens <- mapM fromVariant <=< fromVariant . Variant $ screens
    pure $ FullStatus { fs_workspaces
                      , fs_screens
                      }
  fromVariant _ = Nothing
#endif

#if json
$(deriveJSON defaultOptions{fieldLabelModifier = drop 3} ''FullStatus)

-- | Helper to encode a `ToJSON` to a `Data.String`.
toJsonString :: ToJSON a => a -> String
toJsonString = B.toString . encode

-- | Helper to decode a JSON `Data.String` to a `FullStatus`.
fromJsonString :: String -> Maybe FullStatus
fromJsonString = decode . B.fromString
#endif
