{-# LANGUAGE OverloadedStrings #-}

module Xmobar.Plugins.DBusLog where

import Data.Barstate (FullStatus)
import Xmobar.Data.PP (StatePP)

import Control.Concurrent.Chan (Chan, newChan, writeChan, readChan)
import Control.Monad (forever)
import Text.Regex (Regex, subRegex, mkRegex, matchRegex)
import Xmobar (Config, Exec(..), commands, XPosition(OnScreen), position, Runnable(Run))

import qualified DBus as D
import qualified DBus.Client as DC

-- | A logger, similar to `Xmobar.XMonadLog`, but receives its data
-- via DBus using the `FullStatus` format specified in `Data.Barstate`.
-- The logger can be constructed with a screen ID which is passed to the `StatePP`,
-- facilitating conditional behavior based on whether or not the ID matches.
-- Intended use is through the `configWithDBusLog` hook, which deals with the DBus client setup,
-- however a custom implementation can easily achieved, since `DBusLog` receives its data
-- through the channel that is passed to its constructor.
--
-- Example:
-- > xmobar
-- >   =<< configWithDBus ppState
-- >   =<< configFromArgs def
-- >
-- > ppState :: StatePP FullStatus
-- > ppState = ...
--
-- `<action>` tags are stripped.
data DBusLog t = DBusLog (StatePP t) (Chan t) Int

instance Show (DBusLog t) where
  show (DBusLog _ _ screen) = "DBusLog (StatePP t) (Chan t) " ++ show screen

-- Xmobar requires all "Exec" instances to be constructable from a String/File.
-- Since this is impossible for this type, we throw an error if it happens.
instance Read (DBusLog t) where
  readsPrec _ = error "this type cannot be constructed from a string"

instance Exec (DBusLog t) where
  -- String that can be used in the template string to get a DBusLog.
  alias (DBusLog _ _ _) = "DBusLog"

  start (DBusLog ppState chan screen) callback =
    forever $ (callback . stripActions .  ppState screen) =<< readChan chan

-- | Adds an instance of `DBusLog` to a base Xmobar `Config`.
--
-- NOTE: This function should only be called once!
configWithDBusLog :: StatePP FullStatus -> Config -> IO Config
configWithDBusLog statePP conf = do
  client <- DC.connectSession
  chan <- newChan
  -- Signal needs to be active for the entirety of the programs lifespan
  _ <- DC.addMatch client DC.matchAny { DC.matchPath = Just "/org/finn/barstate"
                                      , DC.matchInterface = Just "org.finn.barstate"
                                      , DC.matchMember = Just "update"
                                      } (\sig -> case D.fromVariant $ D.signalBody sig !! 0 of
                                                   Just state -> writeChan chan state
                                                   _ -> pure ())
  return conf { commands = Run (DBusLog statePP chan screen) : commands conf }
  where
    screen = case position conf of
      OnScreen s _ -> s
      _ -> 0

-- | `stripActions` from `Xmobar.Run.Actions` of the `xmobar` package.
--
-- Copyright: (c) Alexander Polakov
--
-- License: BSD-3-Clause (see LICENSE)
stripActions :: String -> String
stripActions s = case matchRegex actionRegex s of
  Nothing -> s
  Just _  -> stripActions strippedOneLevel
  where
      strippedOneLevel = subRegex actionRegex s "[action=\\1\\2]\\3[/action]"

-- | `actionRegex` from `Xmobar.Run.Actions` of the `xmobar` package.
--
-- Copyright: (c) Alexander Polakov
--
-- License: BSD-3-Clause (see LICENSE)
actionRegex :: Regex
actionRegex = mkRegex "<action=`?([^>`]*)`?( +button=[12345]+)?>(.+)</action>"
