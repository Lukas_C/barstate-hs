module Xmobar.Data.PP where

-- | A function that takes a screen ID and a `FullStatus` and converts it into an Xmobar log string.
--
-- Example:
-- > ppState :: StatePP FullStatus
-- > ppState sid state =
-- >   concat [ "showing on screen: " ++ show sid
-- >          , separator
-- >          , intercalate " " $ map ws_name $ fs_workspace state
-- >          ]
-- >  where
-- >    -- markup also works
-- >    separator = "<fc=#FF0000> | </fc>"
type StatePP t = Int -> t -> String
