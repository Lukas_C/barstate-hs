{
  description = "A library for IPC between a window manager and a status bar.";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable"; };

  outputs = { self, nixpkgs }:
    let pkgs = import nixpkgs { system = "x86_64-linux"; };
    in {
      packages."x86_64-linux".default =
        pkgs.haskellPackages.developPackage { root = ./.; };
    };
}
