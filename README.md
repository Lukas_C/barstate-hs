# Barstate: A Format Agnostic to a Specific Window Manager or Statusbar
## Introduction
This package provides a custom library solution in Haskell for exchanging workspace and screen information between a window manager (WM) and a statusbar ("bar" for short).
Out of the box support for XMonad and Xmobar is included, however the protocol is meant to be independent of the specific WM and bar.
In this regard it is similar to the solution proposed by the "Extended Window Manager Hints" specification (i.e. `_NET_CURRENT_DESTKTOP` and related X11 atoms), however this format includes more information and in a structured format.
Furthermore, it is also designed for use with DBus, which makes it possible to use on both X11 and Wayland, allowing greater interoperability and flexibility in general.

## Contents
[[_TOC_]]

## Quickstart for XMonad and Xmobar
This implementation assumes the user has access to the default DBus user-space session.
### XMonad
Add support in your `xmonad.hs` like so:
``` haskell
import XMonad

import XMonad.Hooks.StatusBar (dynamicEasySBs)
import XMonad.Hooks.StatusBar.Barstate.DBus (statusBarDBus)

main :: IO ()
main = xmonad
  $ dynamicEasySBs (statusBarDBus (\sid -> "xmobar -x" ++ show sid))
  $ def
```

### Xmobar
Add support in your `xmobar.hs` like so:
``` haskell
import Xmobar

import Data.Barstate (FullStatus(..), WorkspaceStatus(..), ScreenStatus(..))
import Xmobar.Data.PP (StatePP)
import Xmobar.Plugins.DBusLog (configWithDBusLog)

import Data.List (intercalate)

ppState :: StatePP FullStatus
ppState sid state =
  concat [ intercalate " " $ map ppWs $ fs_workspaces state
         , separator
         , sc_layout currentScreenState
         , separator
         , sc_windowTitle currentScreenState
         ]
  where
    currentScreenState = (fs_screens state) !! sid

    ppWs ws =
      wrapColor color
      $ uncurry wrapText chars
      $ ws_name ws
      where
        -- Gruvbox-like theme
        color | ws_isUrgent ws            = "#fb4933"
              | ws_isCurrent ws
                && ws_visibleOn ws == sid = "#ebdbb2"
              | ws_visibleOn ws > -1      = "#504945"
              | ws_hasWindows ws          = "#504945"
              | otherwise                 = "#3c3836"

        chars | ws_isUrgent ws         = ("!", "!")
              | ws_visibleOn ws == sid = ("[", "]")
              | ws_visibleOn ws > -1   = ("<", ">")
              | otherwise              = (" ", " ")

wrapColor :: Color -> String -> String
wrapColor color content = "<fc=" ++ color ++ ">" ++ content ++ "</fc>"

wrapText :: String -> String -> String -> String
wrapText l r t = l ++ t ++ r

separator :: String
separator = wrapColor "#ebdbb2" " | "

main :: IO ()
main = do
  xmobar
    =<< configWithDBusLog ppState
    =<< configFromArgs $ defaultConfig { template = "%DBusLog%" }
```

## Format Description
### Starting Example
The following is a generated example JSON dump of a fictional example message (`--` indicates an explanatory comment until the end of the line).
``` json
{
  -- State of all workspaces, independent of whether or not they are currently visible.
  -- SHOULD be sorted in the correct, desired order for easy iteration.
  "workspaces": [
    {
      "name": "main",     -- Workspace name
      "layout": "tall",   -- Workspace layout (for tiling WMs)
      "isCurrent": false, -- The workspace is the current main focus
      "visibleOn": -1,    -- Screen the monitor is visible on. A negative value means invisible.
      "hasWindows": true, -- Workspace contains at least one window.
      "isUrgent": false   -- Workspace contains a window that was flagged as urgent.
    },
    {
      "name": "second",
      "layout": "full",
      "isCurrent": false,
      "visibleOn": 2,
      "hasWindows": true,
      "isUrgent": false
    },
    {
      "name": "third",
      "layout": "tall",
      "isCurrent": false,
      "visibleOn": 1,
      "hasWindows": true,
      "isUrgent": false
    },
    {
      "name": "fourth",
      "layout": "tall",
      "isCurrent": true,
      "visibleOn": 0,
      "hasWindows": true,
      "isUrgent": false
    }
  ],
  -- List of all screens.
  -- SHOULD to be sorted by screen index in ascending order and
  -- SHOULD be indexed from zero contiguously, for straightforward indexing.
  "screens": [
    {
      "sid": 0,                 -- Screen identifier. MAY be truncated to a u16 by the backing implementation.
      "tag": "fourth",          -- Name of the workspace focused on that screen.
      "layout": "tall",         -- Layout of the workspace focused on that screen.
      "windowTitle": "Terminal" -- Window title of the window that was last focused on that workspace.
    },
    {
      "sid": 1,
      "tag": "third",
      "layout": "tall",
      "windowTitle": "README.org - Editor"
    },
    {
      "sid": 2,
      "tag": "browser",
      "layout": "tall",
      "windowTitle": "Extended Window Manager Hints - Browser"
    }
  ]
}
```

Further details on the backing data structures can be found in the `Data.Barstate` module.

### Specification and Considerations
The following should be considered a first draft for a more thorough specification and not at all final.
As I start using this implementation, I might discover additional edge cases that may require an extension or revision.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119.
#### Workspaces
##### Sorting Order
Workspaces SHOULD be ordered in the order that the information is expected to be displayed later by the bar.
This allows the bar to iterate over the list of workspaces directly and without any additional processing to generate a list of widgets already in the correct order for displaying.
A window manager MAY choose to provide an empty list, if the concept of workspaces is not applicable to it.

#### Screens
##### Sorting Order
Screens SHOULD be ordered in ascending screen index order.
Furthermore, the screen identifiers SHOULD be indexed starting from zero and be contiguous.
The implementation MAY insert dummy screens to keep the numbering contiguous and matched to the index position.
In any case, the screen sorting order MUST be kept consistent between messages.
This greatly simplifies fetching the information for a specific screen, when a bar is only interested in the state for the screen it is launched on.

## Building
### Nix
``` sh
nix build
```

### Cabal
``` sh
cabal build --flags "<optional flags>"
```

The flags are described in the `barstate.cabal` file.

Build the documentation with:
``` sh
cabal haddock
```
