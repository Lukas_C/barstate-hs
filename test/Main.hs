{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BangPatterns #-}
module Main (main) where

import Test.HUnit

#if dbus
import DBus.Internal.Types
#endif

#if dbus || json
import Data.Barstate

testData :: FullStatus
testData = FullStatus { fs_workspaces
                          , fs_screens
                          }
  where
    fs_workspaces =
      [ WorkspaceStatus { ws_name = "test_name"
                        , ws_layout = "test_layout"
                        , ws_isCurrent = True
                        , ws_visibleOn = -1
                        , ws_hasWindows = True
                        , ws_isUrgent = True
                        }
      , WorkspaceStatus { ws_name = "test_name2"
                        , ws_layout = "test_layout2"
                        , ws_isCurrent = False
                        , ws_visibleOn = 0
                        , ws_hasWindows = False
                        , ws_isUrgent = False
                        }
      ]
    fs_screens =
      [ ScreenStatus { sc_sid = 0
                     , sc_tag = "test_name2"
                     , sc_layout = "test_layout2"
                     , sc_windowTitle = ""
                     }
      ]
#endif

-- * DBus Tests
dbusTests :: Test
#if dbus
dbusSerialize :: Test
dbusSerialize =
  TestCase (do let
                 -- force strict evaluation
                 !serialized = toVariant testData
                 !deserialized = fromVariant serialized
               assertEqual "deserialization failed" (Just testData) deserialized)

dbusTests = TestList [ dbusSerialize ]
#else
dbusTests = TestList []
#endif

-- * JSON Tests
jsonTests :: Test
#if json
jsonSerialize :: Test
jsonSerialize =
  TestCase (do let
                 -- force strict evaluation
                 !serialized = toJsonString testData
                 !deserialized = fromJsonString serialized
               assertEqual "deserizalization failed" (Just testData) deserialized)

jsonTests = TestList [ jsonSerialize ]
#else
jsonTests = TestList []
#endif


main :: IO ()
main = runTestTTAndExit $ TestList [ dbusTests
                                   , jsonTests
                                   ]
